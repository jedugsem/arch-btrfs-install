create_swap() {
  truncate -s 0 /var/swap/swapfile
  chattr +C /var/swap/swapfile
  fallocate -l 8G /var/swap/swapfile

  chmod 600 /var/swap/swapfile
  mkswap /var/swap/swapfile
  swapon /var/swap/swapfile

  echo "vm.swappiness=10" >> /etc/sysctl.conf
}

setup_snapper() {
  apk add --allow-untrusted "/home/installer/chroot/snapper-0.10.6-r0.apk"
  umount /.snapshots
  rm -r /.snapshots
  snapper --no-dbus -c root create-config /
  btrfs subvolume delete /.snapshots
  mkdir /.snapshots
  mount -o compress=zstd,subvol=@/.snapshots /dev/mapper/cryptroot /.snapshots
  chmod 750 /.snapshots
}

setup_grub() {
  apk add grub-x86_64-efi
  
  # Grub
  sed -i -e "s/rootflags=subvol=${rootsubvol}/    /g" /etc/grub.d/20_linux_xen
  sed -i -e "s/rootflags=subvol=${rootsubvol}/    /g" /etc/grub.d/10_linux

  echo "GRUB_ENABLE_CRYPTODISK=y" >> /etc/default/grub
  echo "GRUB_CMDLINE_LINUX_DEFAULT=\"cryptdevice=UUID=${UUID}:cryptroot:allow-discards resume=/dev/mapper/$CRYPT_NAME resume_offset=${roffset}\"" >> /etc/default/grub


  # Initramfs
  echo "KEYFILE_PATTERN=\"/crypto_keyfile.bin\"" >>/etc/cryptsetup-initramfs/conf-hook
  #echo "RESUME=UUID=${LUKS_UUID}" > /etc/initramfs-tools/conf.d/resume
  echo UMASK=0077 >>/etc/initramfs-tools/initramfs.conf
  
  update-initramfs -c -k all
  
  grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=boot
  grub-mkconfig -o /boot/grub/grub.cfg

}

fstab () {
    echo "cryptroot UUID=${UUID} /crypto_keyfile.bin luks,discard,key-slot=1" > /etc/crypttab
    echo "#
# See fstab(5).
#
# <file system>	<dir>	<type>	<options>		<dump>	<pass>
tmpfs		/tmp	tmpfs	defaults,nosuid,nodev   0       0
UUID=${LUKS_UUID} / btrfs rw,$MOUNT_OPTS 0 0
UUID=${LUKS_UUID} /home btrfs rw,$MOUNT_OPTS,subvol=/@/home 0 0
UUID=${LUKS_UUID} /opt btrfs rw,$MOUNT_OPTS,subvol=/@/opt 0 0
UUID=${LUKS_UUID} /root btrfs rw,$MOUNT_OPTS,subvol=/@/root 0 0
UUID=${LUKS_UUID} /srv btrfs rw,$MOUNT_OPTS,subvol=/@/srv 0 0
UUID=${LUKS_UUID} /tmp btrfs rw,$MOUNT_OPTS,subvol=/@/tmp 0 0
UUID=${LUKS_UUID} /usr/local btrfs rw,$MOUNT_OPTS,subvol=@/usr/local 0 0
UUID=${LUKS_UUID} /var/cache btrfs rw,$MOUNT_OPTS,subvol=@/var/cache 0 0
UUID=${LUKS_UUID} /var/log btrfs rw,$MOUNT_OPTS,subvol=@/var/log 0 0
UUID=${LUKS_UUID} /var/spool btrfs rw,$MOUNT_OPTS,subvol=@/var/spool 0 0
UUID=${LUKS_UUID} /var/lib btrfs rw,$MOUNT_OPTS,subvol=@/var/lib 0 0
UUID=${LUKS_UUID} /var/tmp btrfs rw,$MOUNT_OPTS,subvol=@/var/tmp 0 0
UUID=${LUKS_UUID} /boot/grub btrfs rw,$MOUNT_OPTS,subvol=@/boot/grub 0 0
UUID=${LUKS_UUID} /.snapshots btrfs rw,$MOUNT_OPTS,subvol=@/.snapshots 0 0
UUID=${LUKS_UUID} /var/swap btrfs rw,subvol=@/var/swap 0 0

UUID=${EFI_UUID} /boot/efi vfat defaults 0 0

/var/swap/swapfile    none    swap    defaults    0   0
  " > /etc/fstab
}
