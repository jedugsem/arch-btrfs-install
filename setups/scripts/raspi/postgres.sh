#!/bin/bash
echo "postgres{"
pacman -S postgresql
mkdir /var/lib/postgres
chown 755 /var/lib/postgres
sudo chown postgres /var/lib/postgres

echo "
sudo -i -u postgres
initdb --locale=en_US.UTF-8 --encoding=UTF8 -D /var/lib/postgres/data
createdb fooddata
"
