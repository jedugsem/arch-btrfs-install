#!/bin/bash

ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
hwclock --systohc
sed -i "s/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g" /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf

echo "KEYMAP=us" >> /etc/vconsole.conf

echo "win11" > /etc/hostname

echo "127.0.0.1	localhost
::1		localhost
127.0.1.1	win11.localdomain	win11" > /etc/hosts


umount /.snapshots
rm -r /.snapshots
snapper --no-dbus -c root create-config /
btrfs subvolume delete /.snapshots
mkdir /.snapshots
mount -o compress=zstd,subvol=@/.snapshots /dev/mapper/cryptroot /.snapshots
chmod 750 /.snapshots


sed -i "s/FILES=()/FILES=(\/crypto_keyfile.bin)/g" /etc/mkinitcpio.conf
sed -i "s/HOOKS=(.*)/HOOKS=(base udev autodetect keyboard keymap modconf block encrypt filesystems)/g" /etc/mkinitcpio.conf

# placeholder      $UUID
UUID=$(blkid | grep "/dev/sda2" | sed "s/.* UUID=\"//g" | sed "s/\" TYPE.*//g")
echo $UUID
sed -i "s/GRUB_CMDLINE_LINUX=\"\"/GRUB_CMDLINE_LINUX=\"cryptdevice=UUID=${UUID}:cryptroot\"/g" /etc/default/grub
sed -i "s/loglevel=3 quiet/loglevel=3/g" /etc/default/grub
sed -i "s/#GRUB_ENABLE_CRYPTODISK/GRUB_ENABLE_CRYPTODISK/g" /etc/default/grub

grub-install --target=x86_64-efi --efi-directory=/efi --bootloader-id=boot
mv /efi/EFI/boot/grubx64.efi /efi/EFI/boot/bootx64.efi

grub-mkconfig -o /boot/grub/grub.cfg

useradd -m -G wheel,audio,input,lp,storage,video,sys,network,power me
chsh -s /usr/bin/zsh me

touch /home/me/.zshrc
echo root
passwd
echo me
passwd me

systemctl enable NetworkManager

echo "permit persist :wheel as root
  permit nopass root as me" >> /etc/doas.conf

mkinitcpio -p linux

