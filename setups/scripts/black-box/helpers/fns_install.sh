#!/bin/sh

installv() {
    DRIVE=$1
    MUSL=$2
    mkdir -p /mnt/var/db/xbps/keys
    cp /var/db/xbps/keys/* /mnt/var/db/xbps/keys/


    if $MUSL; then
        xbps-install -Sy -R https://repo-default.voidlinux.org/current/musl -r /mnt $VOID_BASE
    else
        xbps-install -Sy -R https://repo-default.voidlinux.org/current -r /mnt $VOID_BASE
    fi

    mount --rbind /sys /mnt/sys && mount --make-rslave /mnt/sys
    mount --rbind /dev /mnt/dev && mount --make-rslave /mnt/dev
    mount --rbind /proc /mnt/proc && mount --make-rslave /mnt/proc

    cp /etc/resolv.conf /mnt/etc/
}
