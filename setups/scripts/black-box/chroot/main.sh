#!/bin/sh
. /etc/profile
. /tmp/installer/vars
. /tmp/installer/chroot/helpers/fns_user.sh
. /tmp/installer/chroot/helpers/fns_grub.sh
. /tmp/installer/chroot/helpers/fns_fstab.sh
. /tmp/installer/chroot/helpers/fns_snapper.sh
. /tmp/installer/chroot/helpers/fns_locale.sh
. /tmp/installer/chroot/helpers/fns_swap.sh

ln -sfT dash /usr/bin/sh

chown root:root /
chmod 755 /
chmod 000 /boot/volume.key
chmod -R g-rwx,o-rwx /boot

#   FSTAB

LUKS_UUID=$(blkid -s UUID -o value /dev/mapper/$CRYPT_NAME)
UUID=$(blkid -s UUID -o value "$DRIVE"2)
EFI_UUID=$(blkid -s UUID -o value "$DRIVE"128)
fstab 


# Locales

setup_locales

echo "$SWAP swap"
if $SWAP; then
    # it exports roffset snd 
    create_swap
fi

if $SNAPPER; then
   setup_snapper
fi

echo "crypttab"
echo "$CRYPT_NAME UUID=${UUID} /boot/volume.key luks" >> /etc/crypttab

echo "doas setup missing the sudo removal"
echo "permit persist :wheel as root
  permit nopass root as $USERNAME" >> /etc/doas.conf


setup_grub $UUID $SNAPPER

xbps-reconfigure -fa

# Usersetup
echo $HOSTNAME > /etc/hostname
echo "usersetup\n"
echo "root password"
passwd
add_user $USERNAME
chsh -s /usr/bin/sh $USERNAME

# Servicies
echo "Servicies"
ln -s /etc/sv/dbus /etc/runit/runsvdir/default

if $IWD; then
    ln -s /etc/sv/iwd /etc/runit/runsvdir/default/
else
    ln -s /etc/sv/NetworkManager /etc/runit/runsvdir/default/
fi
echo "finsished"
