#!/bin/sh
setup_locales() {
   echo "setting locales"
   echo "LANG=en_US.UTF-8" >> /etc/locale.conf
   if $MUSL; then
       echo "MUSL"
   else
      sed -i 's/^#en_US/en_US/' /etc/default/libc-locales
      xbps-reconfigure -f glibc-locales
   fi

}
