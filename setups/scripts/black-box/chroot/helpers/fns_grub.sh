#!/bin/sh
setup_grub() {
    UUID=$1
    SNAPPER=$2
    SWAP=$3
    xbps-install -y -S grub-x86_64-efi
    #if $SNAPPER; then
    #	    xbps-install -S grub-btrfs
    #fi
    echo "setup grub"

    sed -i "s/rootflags=subvol=${rootsubvol}/    /g" /etc/grub.d/20_linux_xen
    sed -i "s/rootflags=subvol=${rootsubvol}/    /g" /etc/grub.d/10_linux


    sed -i "s/GRUB_CMDLINE_LINUX_DEFAULT=\"loglevel=4\"/GRUB_CMDLINE_LINUX_DEFAULT=\"rd.auto=1 loglevel=4\"/g" /etc/default/grub

    echo "GRUB_ENABLE_CRYPTODISK=y" >> /etc/default/grub
 if $SWAP; then   
echo "GRUB_CMDLINE_LINUX=\"cryptdevice=UUID=${UUID}:cryptroot resume=/dev/mapper/$CRYPT_NAME resume_offset=${roffset}\"" >> /etc/default/grub
 else

echo "GRUB_CMDLINE_LINUX=\"cryptdevice=UUID=${UUID}:cryptroot\"" >> /etc/default/grub
fi  
 touch /etc/dracut.conf.d/10-crypt.conf
    echo "install_items+=\" /boot/volume.key /etc/crypttab \"" >> /etc/dracut.conf.d/10-crypt.conf

    grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=boot
    cp /boot/efi/EFI/boot/grubx64.efi /boot/efi/EFI/boot/bootx64.efi

}
