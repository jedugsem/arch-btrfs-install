#!/bin/sh
create_swap() {
    echo "creating swapfile"
    xbps-install -y -S gcc
    truncate -s 0 /var/swap/swapfile
    chattr +C /var/swap/swapfile
    #btrfs property set /var/swap/swapfile compression none
    fallocate -l 8G /var/swap/swapfile
    chmod 600 /var/swap/swapfile
    mkswap /var/swap/swapfile
    swapon /var/swap/swapfile
    echo "vm.swappiness=10" >> /etc/sysctl.conf
    gcc -O2 -o /tmp/installer/chroot/btrfs_map_physical /tmp/installer/chroot/btrfs_map_physical.c 
    offset=$(/tmp/installer/chroot/btrfs_map_physical /var/swap/swapfile | head -n2 | awk '/\S/{ s=$NF; } END{ print(s); }')
    roffset=$(expr ${offset} / 4096)
    echo "/var/swap/swapfile    none    swap    defaults    0   0" >> /etc/fstab
}
