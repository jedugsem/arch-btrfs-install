#!/bin/sh
. ./vars
. ./helpers/fns_disk.sh
. ./helpers/fns_install.sh
echo "dependencieces: cryptsetup fdisk"
echo "format your partition to contain sdx128 as boot and sdx2 as root"

ROOT="/mnt"

create_part $DRIVE $CRYPT_NAME $CRYPT_ITER 

mount_part $DRIVE $CRYPT_NAME $MOUNT_OPTS

installv $DRIVE $MUSL 

mkdir $ROOT/tmp/installer
cp -r ./* $ROOT/tmp/installer/
chroot $ROOT /bin/sh /tmp/installer/chroot/main.sh

#umount -R /mnt
#umount /mnt
