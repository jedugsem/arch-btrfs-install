#!/bin/bash
apk add snapper grub zsh libuser doas mkinitfs musl-locales musl-locales-lang grub grub-efi efibootmgr

sed -i "s/subvolid=.*,//g" /etc/fstab
sed -i "s/,subvol=.@..snapshots.1.snapshot/    /g" /etc/fstab
sed -i "s/rootflags=subvol=${rootsubvol}/    /g" /etc/grub.d/20_linux_xen
sed -i "s/rootflags=subvol=${rootsubvol}/    /g" /etc/grub.d/10_linux

sed -i "s/^h/# h/g" /etc/apk/repositories
sed -i "s/#h/h/g" /etc/apk/repositories
apk update

umount /.snapshots
rm -r /.snapshots
snapper --no-dbus -c root create-config /
btrfs subvolume delete /.snapshots
mkdir /.snapshots
mount -o compress=zstd,subvol=@/.snapshots /dev/mapper/root /.snapshots
chmod 750 /.snapshots

# placeholder      $UUID
UUID=$(blkid | grep "/dev/sda2" | sed "s/.* UUID=\"//g" | sed "s/\" TYPE.*//g")

sed -i "s/cryptsetup/cryptkey cryptsetup/" /etc/mkinitfs/mkinitfs.conf
sed -i "s/quiet/cryptkey/" /etc/default/grub
echo "GRUB_ENABLE_CRYPTODISK=y" >> /etc/default/grub

grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=boot
mv /boot/efi/EFI/boot/grubx64.efi /boot/efi/EFI/boot/bootx64.efi
rm -r /boot/boot
mkinitfs -o /boot/initramfs-lts
grub-mkconfig -o /boot/grub/grub.cfg

echo root
passwd

echo "permit persist :wheel as root
  permit nopass root as me" >> /etc/doas.d/doas.conf
