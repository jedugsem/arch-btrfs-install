rm -r .config
rm .zshrc
echo ".river-dots" >> .gitignore
git clone --bare https://www.gitlab.com/jedugsem/river-dots $HOME/.river-dots
git --git-dir=$HOME/.river-dots/ --work-tree=$HOME checkout
git --git-dir=$HOME/.river-dots/ --work-tree=$HOME config --local status.showUntrackedFiles no

git clone https://gitlab.com/dwt1/wallpapers
wal -i wallpapers/0001.jpg

mkdir -p ~/.icons/default
ln -s /usr/share/icons/Adwaita/cursors .icons/default/cursors

xdg-mime default org.pwmt.zathura.desktop application/pdf

cargo install --git https://gitlab.com/snakedye/kile

#zsh -c "git config --global user.email \"jedugsem@gmail.com\"
#git config init.defaultBranch main
#git config --global user.name \"jedugsem\"

abuild-keygen -a -i

doas lchsh me
