#!/bin/sh
create_swap() {
    echo "creating swapfile"
    
    truncate -s 0 /var/swap/swapfile
    chattr +C /var/swap/swapfile
    fallocate -l 8G /var/swap/swapfile

    chmod 600 /var/swap/swapfile
    mkswap /var/swap/swapfile
    swapon /var/swap/swapfile

    echo "vm.swappiness=10" >> /etc/sysctl.conf
    
    roffset=$(btrfs inspect-internal map-swapfile -r /var/swap/swapfile)
    
    echo "/var/swap/swapfile    none    swap    defaults    0   0" >> /etc/fstab
}
