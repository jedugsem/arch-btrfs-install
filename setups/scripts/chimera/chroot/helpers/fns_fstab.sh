#!/bin/sh
fstab () {
    echo "cryptroot UUID=${UUID} /crypto_keyfile.bin luks,discard,key-slot=1" > /etc/crypttab
    echo "filling fstab \n\n"
    echo "#
# See fstab(5).
#
# <file system>	<dir>	<type>	<options>		<dump>	<pass>
tmpfs		/tmp	tmpfs	defaults,nosuid,nodev   0       0
UUID=${LUKS_UUID} / btrfs rw,relatime,$MOUNT_OPTS,ssd,space_cache=v2 0 0
UUID=${LUKS_UUID} /home btrfs rw,relatime,$MOUNT_OPTS,ssd,space_cache=v2,subvol=/@/home 0 0
UUID=${LUKS_UUID} /opt btrfs rw,relatime,$MOUNT_OPTS,ssd,space_cache=v2,subvol=/@/opt 0 0
UUID=${LUKS_UUID} /root btrfs rw,relatime,$MOUNT_OPTS,ssd,space_cache=v2,subvol=/@/root 0 0
UUID=${LUKS_UUID} /srv btrfs rw,relatime,$MOUNT_OPTS,ssd,space_cache=v2,subvol=/@/srv 0 0
UUID=${LUKS_UUID} /tmp btrfs rw,relatime,$MOUNT_OPTS,ssd,space_cache=v2,subvol=/@/tmp 0 0
UUID=${LUKS_UUID} /usr/local btrfs rw,relatime,$MOUNT_OPTS,ssd,space_cache=v2,subvol=@/usr/local 0 0
UUID=${LUKS_UUID} /var/cache btrfs rw,relatime,$MOUNT_OPTS,ssd,space_cache=v2,subvol=@/var/cache 0 0
UUID=${LUKS_UUID} /var/log btrfs rw,relatime,$MOUNT_OPTS,ssd,space_cache=v2,subvol=@/var/log 0 0
UUID=${LUKS_UUID} /var/spool btrfs rw,relatime,$MOUNT_OPTS,ssd,space_cache=v2,subvol=@/var/spool 0 0
UUID=${LUKS_UUID} /var/lib btrfs rw,relatime,$MOUNT_OPTS,ssd,space_cache=v2,subvol=@/var/lib 0 0
UUID=${LUKS_UUID} /var/tmp btrfs rw,relatime,$MOUNT_OPTS,ssd,space_cache=v2,subvol=@/var/tmp 0 0
UUID=${LUKS_UUID} /var/swap btrfs rw,relatime,ssd,space_cache=v2,subvol=@/var/swap 0 0
UUID=${LUKS_UUID} /boot/grub btrfs rw,relatime,$MOUNT_OPTS,ssd,space_cache=v2,subvol=@/boot/grub 0 0
UUID=${LUKS_UUID} /.snapshots btrfs rw,relatime,$MOUNT_OPTS,ssd,space_cache=v2,subvol=@/.snapshots 0 0
UUID=${EFI_UUID} /boot/efi vfat rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=iso8859-1,shortname=mixed,utf8,errors=remount-ro 0 0
" > /etc/fstab

}
