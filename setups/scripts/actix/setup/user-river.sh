BASE="zsh doas neovim zig lolcat starship exa dash xdg-user-dirs pigz git zsh-syntax-highlighting youtube-dl grub-btrfs snap-pac snap-sync"
WAYLAND="swaybg bemenu wev dunst wob wtype clipman swaylock-effects swayidle gvfs waybar wlr-randr"
CLI="brightnessctl ncmpcpp pass torsocks neomutt tectonic bottom"
SERVICES="cups system-config-printer keyd mpd"
GUI="alacritty qutebrowser papirus-icon-theme ttf-font-awesome zathura imv pavucontrol zathura-pdf-mupdf rofi engrampa xfce-polkit wdisplays telegram-desktop"
QT="kvantum"
DEV="rust-analyzer texlab virt-manager qemu pmbootstrap"
NETWORK="network-manager-applet"
EXTRA="wireshark blueman firefox"
PIPEWIRE="pw-volume rtkit pipewire pipewire-pulse pipewire-alsa wireplumber"
# iwgtk

APK="arc-gtk2 arc-gtk3 arc-gtk4 rust rust-wasm pcmanfm py3-psutil py3-daemon py3-tldextract py3-send2trash py3-pywal inotify-tools py3-pynim libindicator font-ubuntu-mono-nerd alpine-sdk mesa-dri-gallium eudev river river-doc mandoc cargo dbus-x11 toapk mesa-vulkan-intel pciutils-libs alsa-lib-dev libxkbcommon-dev wayland-dev doas-sudo-shim pipewire-tools"
PAC="arc-gtk-theme rustup pcmanfm-gtk3 libindicator-gtk3 nerd-fonts-ubuntu-mono tor-runit kile-wl qt5-wayland python-prctl python-psutil python-inotify-simple python-daemon python-tldextract pywal"

INSTALL="$BASE $CLI $WAYLAND $SERVICES $CLI $DEV $NETWORK $QT $EXTRA $PIPEWIRE $GUI"

VOID=""
BSD=""
#apk add $BASE $APK
paru -S $INSTALL $PAC
#xpbs-install $BASE $VOID
#pkg install $BASE $BSD



rm -r .config
rm .zshrc
echo ".river-dots" >> .gitignore
git clone --bare https://www.gitlab.com/jedugsem/river-dots $HOME/.river-dots
git --git-dir=$HOME/.river-dots/ --work-tree=$HOME checkout
git --git-dir=$HOME/.river-dots/ --work-tree=$HOME config --local status.showUntrackedFiles no

git clone https://gitlab.com/dwt1/wallpapers
wal -i wallpapers/0001.jpg

mkdir -p ~/.icons/default
#ln -s /usr/share/icons/Adwaita/cursors .icons/default/cursors

xdg-mime default org.pwmt.zathura.desktop application/pdf

