#!/bin/sh
. ./pkg_vars
paru -S $(echo $AUR)
sleep 4
rm -fr .config
rm -f .zshrc
rm -f .profile
echo ".river-dots" >> .gitignore
git clone --bare https://gitlab.com/jedugsem/river-dots $HOME/.river-dots
git --git-dir=$HOME/.river-dots/ --work-tree=$HOME checkout
git --git-dir=$HOME/.river-dots/ --work-tree=$HOME config --local status.showUntrackedFiles no

git clone https://gitlab.com/dwt1/wallpapers $HOME/.config/wallpapers
#wal -i $HOME/.config/wallpapers/0001.jpg

mkdir -p ~/.icons/default
ln -s /usr/share/icons/Adwaita/cursors ~/.icons/default/

#xdg-mime default org.pwmt.zathura.desktop application/pdf



#/etc/rc.conf FONT=/usr/share/kbd/consolefonts

#/etc/rc.local zsh and stuff right before login

#ln -sf /usr/share/zoneinfo/<timezone> /etc/localtime

#export TZ=<timezone>

#The chrony package provides Chrony and the chronyd service

#/etc/gtk-3.0/settings.ini or .config/gtk-3.0/

#cups-pk-helper
#rustup-init
git clone --depth 1 https://github.com/wbthomason/packer.nvim\
 ~/.local/share/nvim/site/pack/packer/start/packer.nvim

cargo install --git https://github.com/MaxVerevkin/i3bar-river
cargo install --git https://github.com/greshake/i3status-rust i3status-rs
#cargo install --git 
wal -i .config/wallpapers/0302.jpg
nvim -c PackerSync
nvim -c PackerSync
echo "gpg --import Desktop/backup_2_2023/files/backupkeys.pgp"
echo "gpg --output backupkeys.pgp --armor --export-options export-backup --export-secret-keys me@best.org"
