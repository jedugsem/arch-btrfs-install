# /home/me
mkdir ~/Downloads
mkdir ~/Desktop
projects=~/jedugsem/projects
mkdir -p $projects
mkdir -p $projects/server
mkdir -p $projects/jagd
mkdir -p $projects/koop
mkdir -p $projects/games
git clone https://gitlab.com/jedugsem/Documents ~/Documents
git clone https://gitlab.com/jedugsem/Templates ~/Templates
git clone https://gitlab.com/jedugsem/password-store ~/.password-store

git clone https://gitlab.com/jedugsem/raspi $projects/server/raspi
git clone https://gitlab.com/jedugsem/tank-game $projects/games/tank-game
git clone https://gitlab.com/jedugsem/jagenlernen $projects/jagd/jagenlernen
git clone https://gitlab.com/jedugsem/foodserver $projects/koop/foodserver
git clone https://gitlab.com/jedugsem/foodfrontend $projects/koop/foodfrontend
